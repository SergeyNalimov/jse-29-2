package tm;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MathService {
    public long sum(String arg1, String arg2) {
        return parseToLong(arg1) + parseToLong(arg2);
    }

    public long factorial(String arg) {
        Integer parsedArg = parseToInt(arg);
        isPositive(parsedArg);

        long result = 1;

        for (int i = 1; i <= parsedArg; i++) {
            try {
                result = Math.multiplyExact(result, i);
            } catch (ArithmeticException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
        }

        return result;
    }

    public long[] fibonacci(String arg) {
        Long parsedArg = parseToLong(arg);
        isPositive(parsedArg);

        List<Long> sequence = new ArrayList<>();
        sequence.add(0L);
        long sum = 0;
        int n = 1;

        while (sum < parsedArg) {
            sequence.add(fibonacciNumber(++n));
            sum = sequence.stream().collect(Collectors.summingLong(Long::longValue));
        }

        if (sum == parsedArg) {
            return sequence.stream().mapToLong(Long::longValue).toArray();
        } else {
            throw new IllegalArgumentException("there is no sequence for the argument " + arg);
        }
    }

    private long fibonacciNumber(int n) {
        if (n == 1) {
            return 0L;
        }
        if (n == 2) {
            return 1L;
        }
        return fibonacciNumber(n - 1) + fibonacciNumber(n - 2);
    }

    private int parseToInt(String number) {
        try {
            return Integer.parseInt(number);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    private long parseToLong(String number) {
        try {
            return Long.parseLong(number);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    private void isPositive(int number) {
        if (number <= 0) {
            throw new IllegalArgumentException(String.format("Number %d < 0", number));
        }
    }

    private void isPositive(long number) {
        if (number <= 0) {
            throw new IllegalArgumentException(String.format("Number %f < 0", number));
        }
    }
}

